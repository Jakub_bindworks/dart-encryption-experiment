import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:core';
import 'dart:math';
import 'dart:typed_data';

import 'package:pointycastle/api.dart';

// neco co generuje potencialne obrovsky zdroj dat
// (asi nejaky for (SELECT LIMIT ...))
Stream<String> dataSource() async* {
  for (var i = 0; i < 100000; i ++) {
    yield "Number $i\n";
  }
}

// funkce, ktera ze streamu bloku libovolne velikosti udela stream bloku velikosti blockSize
// a posledni blok muze byt mensi
Stream<List<int>> makeBlocks(Stream<List<int>> source, int blockSize) async* {
  var block = Uint8List(blockSize);
  var blockOffset = 0;

  await for (var chunk in source) {
    var chunkOffset = 0;
    while (chunkOffset < chunk.length) {
      var chunkBitToAddToBlock = min(chunk.length - chunkOffset, blockSize - blockOffset);

      block.setRange(blockOffset, blockOffset + chunkBitToAddToBlock, chunk.sublist(chunkOffset));
      blockOffset += chunkBitToAddToBlock;
      chunkOffset += chunkBitToAddToBlock;
      
      if (blockOffset == blockSize) {
        yield block;
        blockOffset = 0;
      }
    }
  }

  if (blockOffset > 0) {
    yield block.sublist(0, blockOffset);
  }
}

// funkce, ktera akceptuje stream bloku velikosti co ma cipher blok (az na posledni)
// a ten zakrypti pomoci dane sifry
Stream<List<int>> encrypt(Stream<List<int>> source, PaddedBlockCipher cipher) async* {
  var encryptedBlock = Uint8List(cipher.blockSize * 2);

  await for (var chunk in source) {
    if (chunk.length == cipher.blockSize) {
      var bytes = cipher.processBlock(chunk, 0, encryptedBlock, 0);
      yield encryptedBlock.sublist(0, bytes);
    } else {
      // je potreba rozlisovat stav, kdy prisel posledni, kratsi blok ...
      var bytes = cipher.doFinal(chunk, 0, encryptedBlock, 0);
      yield encryptedBlock.sublist(0, bytes);
      return;
    }
  }

  // ... a nebo byl posledni blok presne veliky a vlastne zadny "dalsi" uz neni
  var bytes = cipher.doFinal(Uint8List(0), 0, encryptedBlock, 0);
  yield encryptedBlock.sublist(0, bytes);
}

void main(List<String> arguments) async {
  var out = File('out.encrypted.bin');
  var sink = out.openWrite();
  var cipher = PaddedBlockCipher("AES/CBC/PKCS7");
  var key = Uint8List.fromList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);
  var iv = Uint8List.fromList([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]);

  cipher.init(true, PaddedBlockCipherParameters(ParametersWithIV(KeyParameter(key), iv), null));
  
  await dataSource() // nejakym zpusobem generuju Stream<String>u, asi selektama do databaze nebo neco :-)
    .transform(utf8.encoder) // prevedu na List<int> abych mel jasne bytes
    .transform(GZipCodec().encoder)
    .transform(StreamTransformer.fromBind((s) { return makeBlocks(s, cipher.blockSize); })) // vyrobim z toho bloky spravne velikosti
    .transform(StreamTransformer.fromBind((s) { return encrypt(s, cipher); })) // a ty zakryptim
    .forEach((element) { sink.add(element); }); // nakonec to vsechno jede do souboru

  sink.close();
}
