Run:

```
dart bin/encryption_experiment.dart
```

this will produce `out.encrypted.bin` which is encrypted

try to decrypt it with openssl:

```
openssl aes-128-cbc -K 000102030405060708090a0b0c0d0e0f -iv 000102030405060708090a0b0c0d0e0f -d -in out.encrypted.bin | gunzip -dc
```

it works! :-)
